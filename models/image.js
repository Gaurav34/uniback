


const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const imgLikes = new Schema(
  {
    imagurl: String,
  
    userEmail: String,
    likes: Number,
    likesBy:Array
 
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("imglikes", imgLikes);

